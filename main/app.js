function addTokens(input, tokens) {
    if (typeof input !== "string")
        throw new Error("Invalid input");
  
    if (input.length < 6)
        throw new Error("Input should have at least 6 characters");
  
   
    function validareTokens(tokens) {
        for (token of tokens) {
            let values = Object.values(token);
            if (values.length > 1 || typeof values[0] !== "string")
                return false;
        }
        return true;
    }
    if (!validareTokens(tokens))
        throw new Error("Invalid array format");

    if (input.includes('...') && input.match(/\.\.\./g).length === tokens.length)
        tokens.forEach(token => input = input.replace('...', "${"+Object.values(token)[0])+"}");
    return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;